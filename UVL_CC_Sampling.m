classdef UVL_CC_Sampling
    
    properties

        PatternImage
        PatternImageRAW
        PatternImageSamples
        PatternImagePath
        PatternCorners
        ReferenceGridCorners
        ReferenceGridCenters
        ReferenceGrid
        ProjectedGrid
        NRows
        NColumns
        AvgSize
        AreaShape
        hCurrentFigure
        NSamples
        NSamplesList
        SampleAlternation
        
    end
    
    methods

        function obj = UVL_CC_Sampling

            obj.PatternImage = [];
            obj.PatternImageRAW = [];
            obj.PatternImageSamples = [];
            obj.PatternImagePath = [];
            obj.PatternCorners = [];
            obj.ReferenceGridCenters.x = [];
            obj.ReferenceGridCenters.y = [];
            obj.ReferenceGrid.Center.x = [];
            obj.ReferenceGrid.Center.y = [];
            obj.NRows = 5;
            obj.NColumns = 5;
            obj.AvgSize = 0.25;
            obj.AreaShape = 'Square';
            obj.NSamples = 0;
            obj.NSamplesList = [];
            obj.SampleAlternation = 'None';

        end
        
        function n = nSquares(obj)

            n = obj.NRows * obj.NColumns;
            
        end
        
        function g = grid(obj)
            
            g = [obj.NRows 1; 2 2];
            
        end
        
        function obj = loadPatternImage(obj, ImagePath)

            obj.PatternImagePath = ImagePath;
            [pathstr, name, ext] = fileparts(obj.PatternImagePath);
            
            if strcmpi(ext, '.cr2')

                % Convert Image from CR2 to 16 bit RAW TIFF
                system(['.\\dcraw\\dcraw64.exe -v -q 3 -o 2 -T ' pathstr '\' name ext]);
                system(['move /y ' pathstr '\' name  '.tiff ' pathstr '\' name '_preview.tiff']);
                system(['.\\dcraw\\dcraw64.exe -v -q 3 -o 0 -4 -T -H 0 -W ' pathstr '\' name ext]);

                obj.PatternImage = imread([pathstr '\\' name '_preview.tiff']);
                obj.PatternImageRAW = imread([pathstr '\\' name '.tiff']);
                
            else

                obj.PatternImage = imread(obj.PatternImagePath);
                obj.PatternImageRAW = obj.PatternImage;
                
            end

        end
        
        function obj = clickPatternCorners(obj)

            fprintf('Click the four outer corners of the pattern in clock-wise order starting on the top-left\n');

            warning off images:initSize:adjustingMag
            if numel(obj.hCurrentFigure) == 0
                obj.hCurrentFigure = figure(); imshow(obj.PatternImage); hold on
            else
                figure(obj.hCurrentFigure); hold off; imshow(obj.PatternImage); hold on
            end
            warning on images:initSize:adjustingMag

            obj.PatternCorners = [];
            
            while size(obj.PatternCorners, 1) < 4
                [x, y, Button] = ginput(1);
                
                if Button == 1
                    obj.PatternCorners = [obj.PatternCorners; x y];
                    plot(x, y, '+g');
                    text(x + 10, y + 10, num2str(size(obj.PatternCorners, 1)), 'Color', 'green');
                else
                    obj.PatternCorners = [];
                    hold off; imshow(obj.PatternImage); hold on
                end
            end
            
            plot([obj.PatternCorners(1, 1) obj.PatternCorners(2, 1) obj.PatternCorners(3, 1) obj.PatternCorners(4, 1) obj.PatternCorners(1, 1)], ...
                 [obj.PatternCorners(1, 2) obj.PatternCorners(2, 2) obj.PatternCorners(3, 2) obj.PatternCorners(4, 2) obj.PatternCorners(1, 2)], '-r');

            % Reference Grid Corners
            obj.ReferenceGridCorners.x = [1 obj.NColumns+1 obj.NColumns+1              1];
            obj.ReferenceGridCorners.y = [1              1 obj.NRows+1    obj.NRows+1];
            
            % Reference Gric Centers and Square Corners
            [xc, yc] = meshgrid(1 : obj.NColumns, 1 : obj.NRows);

            xc = xc(:) + 0.5;
            yc = yc(:) + 0.5;
            
            obj.NSamples = numel(xc);
            
            for i = 1 : obj.NSamples

                obj.ReferenceGrid(i).Center.x = xc(i);
                obj.ReferenceGrid(i).Center.y = yc(i);
                
                if obj.AreaShape == 'Square'
                    obj.ReferenceGrid(i).Corners.x = [xc(i)-obj.AvgSize xc(i)+obj.AvgSize xc(i)+obj.AvgSize xc(i)-obj.AvgSize];
                    obj.ReferenceGrid(i).Corners.y = [yc(i)-obj.AvgSize yc(i)-obj.AvgSize yc(i)+obj.AvgSize yc(i)+obj.AvgSize];
                else
                    th = 0 : pi/30 : 2*pi;
                    obj.ReferenceGrid(i).Corners.x = obj.AvgSize * cos(th) + xc(i);
                    obj.ReferenceGrid(i).Corners.y = obj.AvgSize * sin(th) + yc(i);                  
                end
                
            end

            H = ComputeProjectiveTransformation([obj.ReferenceGridCorners.x; obj.ReferenceGridCorners.y], obj.PatternCorners');
            obj.NSamplesList = [];
            
            for i = 1 : obj.NSamples
                
                ProjectedCenter = H * [obj.ReferenceGrid(i).Center.x; obj.ReferenceGrid(i).Center.y; 1.0];
                ProjectedCenter = ProjectedCenter ./ ProjectedCenter(3);

                obj.ProjectedGrid(i).Center.x = ProjectedCenter(1);
                obj.ProjectedGrid(i).Center.y = ProjectedCenter(2);

                ProjectedCorners = H * [obj.ReferenceGrid(i).Corners.x; obj.ReferenceGrid(i).Corners.y; ones(1, numel(obj.ReferenceGrid(i).Corners.x))];
                ProjectedCorners = ProjectedCorners ./ ProjectedCorners(3, :);
                
                obj.ProjectedGrid(i).Corners.x = ProjectedCorners(1, :);
                obj.ProjectedGrid(i).Corners.y = ProjectedCorners(2, :);
                
                figure(obj.hCurrentFigure);
                
                switch obj.SampleAlternation
                    case 'None'
                        plot([obj.ProjectedGrid(i).Corners.x obj.ProjectedGrid(i).Corners.x(1)], [obj.ProjectedGrid(i).Corners.y obj.ProjectedGrid(i).Corners.y(1)], '-g'); drawnow
                        obj.NSamplesList = [obj.NSamplesList i];
                    case 'Even'
                        if ~mod(i, 2)
                            plot([obj.ProjectedGrid(i).Corners.x obj.ProjectedGrid(i).Corners.x(1)], [obj.ProjectedGrid(i).Corners.y obj.ProjectedGrid(i).Corners.y(1)], '-g'); drawnow
                            obj.NSamplesList = [obj.NSamplesList i];

                        end
                    case 'Odd'
                        if mod(i, 2)
                            plot([obj.ProjectedGrid(i).Corners.x obj.ProjectedGrid(i).Corners.x(1)], [obj.ProjectedGrid(i).Corners.y obj.ProjectedGrid(i).Corners.y(1)], '-g'); drawnow
                            obj.NSamplesList = [obj.NSamplesList i];
                        end
                end
                
            end
             
        end

        function obj = extractSamples(obj)

            obj.PatternImageSamples = zeros(size(obj.PatternImage, 1), size(obj.PatternImage, 2));

            for i = obj.NSamplesList

                MaskAux = poly2mask(obj.ProjectedGrid(i).Corners.x, obj.ProjectedGrid(i).Corners.y, size(obj.PatternImage, 1), size(obj.PatternImage, 2));
                obj.PatternImageSamples = obj.PatternImageSamples + MaskAux * i;

            end
            
            % Store color patches
            for i = obj.NSamplesList
                
                [y, x] = find(obj.PatternImageSamples == i);
                
                [pathstr, name, ~] = fileparts(obj.PatternImagePath);
                col = floor((i - 1) / obj.NRows) + 1;
                row = mod(i - 1, obj.NRows) + 1;
                
                PatchFileName = sprintf('%s\\%s\\%s_%04d_%04d.txt', pathstr, name, name, row, col);
                
                if ~exist(sprintf('%s\\%s', pathstr, name), 'dir')
                    mkdir(sprintf('%s\\%s', pathstr, name));
                end
                
                fid = fopen(PatchFileName, 'wt');
                fprintf('Storing Patch %s\n', PatchFileName);

                for j = 1 : numel(y)
                    fprintf(fid, '%05d %05d %05d\n', obj.PatternImageRAW(y(j), x(j), :));
                end
                
                fclose(fid);
                
            end
            
        end

    end

end
