function varargout = ColorSampling(varargin)
% COLORSAMPLING MATLAB code for ColorSampling.fig
%      COLORSAMPLING, by itself, creates a new COLORSAMPLING or raises the existing
%      singleton*.
%
%      H = COLORSAMPLING returns the handle to a new COLORSAMPLING or the handle to
%      the existing singleton*.
%
%      COLORSAMPLING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COLORSAMPLING.M with the given input arguments.
%
%      COLORSAMPLING('Property','Value',...) creates a new COLORSAMPLING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ColorSampling_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ColorSampling_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ColorSampling

% Last Modified by GUIDE v2.5 20-Jan-2017 12:15:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ColorSampling_OpeningFcn, ...
                   'gui_OutputFcn',  @ColorSampling_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ColorSampling is made visible.
function ColorSampling_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ColorSampling (see VARARGIN)

% Choose default command line output for ColorSampling
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ColorSampling wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ColorSampling_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function EditTextPatternFile_Callback(hObject, eventdata, handles)
% hObject    handle to EditTextPatternFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditTextPatternFile as text
%        str2double(get(hObject,'String')) returns contents of EditTextPatternFile as a double


% --- Executes during object creation, after setting all properties.
function EditTextPatternFile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditTextPatternFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in PushButtonLoadPattern.
function PushButtonLoadPattern_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonLoadPattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ColorPatternSampling;

ColorPatternSampling = UVL_CC_Sampling;

[PatternFile, PatternPath] = uigetfile('*.jpg; *.cr2', 'Load Color Calibration Pattern', 'E:\Development\Matlab\ColorCalibration\');

if PatternFile
    fprintf('Pattern File Name: %s\n', [PatternPath PatternFile]);
    handles.EditTextPatternFile.String = [PatternPath PatternFile]; drawnow
    set(handles.PushButtonClickCorners, 'Enable', 'on');
    
    handles.EditTextProcessingStatus.String = 'Loading Pattern Image'; drawnow
    ColorPatternSampling = ColorPatternSampling.loadPatternImage([PatternPath PatternFile]);
    handles.EditTextProcessingStatus.String = 'Pattern Image Loaded Successfully'; drawnow
    
end



% --- Executes on slider movement.
function SliderPatternRows_Callback(hObject, eventdata, handles)
% hObject    handle to SliderPatternRows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.EditTextPatternRows.String = num2str(get(hObject, 'Value'));

if handles.CheckBoxSquarePattern.Value
    set(handles.SliderPatternColumns, 'Value', get(hObject, 'Value'));
    handles.EditTextPatternColumns.String = num2str(get(hObject, 'Value'));
end


% --- Executes during object creation, after setting all properties.
function SliderPatternRows_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SliderPatternRows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function SliderPatternColumns_Callback(hObject, eventdata, handles)
% hObject    handle to SliderPatternColumns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.EditTextPatternColumns.String = num2str(get(hObject, 'Value'));

if handles.CheckBoxSquarePattern.Value
    set(handles.EditTextPatternRows, 'Value', get(hObject, 'Value'));
    handles.EditTextPatternRows.String = num2str(get(hObject, 'Value'));
end


% --- Executes during object creation, after setting all properties.
function SliderPatternColumns_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SliderPatternColumns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function EditTextPatternRows_Callback(hObject, eventdata, handles)
% hObject    handle to EditTextPatternRows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditTextPatternRows as text
%        str2double(get(hObject,'String')) returns contents of EditTextPatternRows as a double


% --- Executes during object creation, after setting all properties.
function EditTextPatternRows_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditTextPatternRows (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function EditTextPatternColumns_Callback(hObject, eventdata, handles)
% hObject    handle to EditTextPatternColumns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditTextPatternColumns as text
%        str2double(get(hObject,'String')) returns contents of EditTextPatternColumns as a double


% --- Executes during object creation, after setting all properties.
function EditTextPatternColumns_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditTextPatternColumns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in PushButtonExtractSamples.
function PushButtonExtractSamples_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonExtractSamples (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ColorPatternSampling;

handles.EditTextProcessingStatus.String = 'Extracting and Storing Samples'; drawnow
ColorPatternSampling = ColorPatternSampling.extractSamples;
handles.EditTextProcessingStatus.String = 'Samples Stored Successfully'; drawnow


% --- Executes on button press in PushButtonClickCorners.
function PushButtonClickCorners_Callback(hObject, eventdata, handles)
% hObject    handle to PushButtonClickCorners (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ColorPatternSampling;

ColorPatternSampling.NRows = get(handles.SliderPatternRows, 'Value');
ColorPatternSampling.NColumns = get(handles.SliderPatternColumns, 'Value');

ColorPatternSampling.AvgSize = round(handles.SliderSampleSize.Value / 2.0) / 100;
ColorPatternSampling.AreaShape = handles.PopUpMenuSampleShape.String{handles.PopUpMenuSampleShape.Value};
ColorPatternSampling.SampleAlternation = handles.PopUpMenuSampleAlternation.String{handles.PopUpMenuSampleAlternation.Value};

handles.EditTextProcessingStatus.String = 'Click the four outer corners of the pattern in clock-wise order starting on the top-left'; drawnow
ColorPatternSampling = ColorPatternSampling.clickPatternCorners;
set(handles.PushButtonExtractSamples, 'Enable', 'on');
handles.EditTextProcessingStatus.String = 'Corners Clicked Successfully'; drawnow


function EditTextProcessingStatus_Callback(hObject, eventdata, handles)
% hObject    handle to EditTextProcessingStatus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditTextProcessingStatus as text
%        str2double(get(hObject,'String')) returns contents of EditTextProcessingStatus as a double


% --- Executes during object creation, after setting all properties.
function EditTextProcessingStatus_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditTextProcessingStatus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CheckBoxSquarePattern.
function CheckBoxSquarePattern_Callback(hObject, eventdata, handles)
% hObject    handle to CheckBoxSquarePattern (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckBoxSquarePattern

if get(hObject, 'Value')
    set(handles.SliderPatternColumns, 'Value', get(handles.SliderPatternRows, 'Value'));
    handles.EditTextPatternColumns.String = num2str(get(handles.SliderPatternRows, 'Value'));
end


% --- Executes on slider movement.
function SliderSampleSize_Callback(hObject, eventdata, handles)
% hObject    handle to SliderSampleSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global ColorPatternSampling;

handles.EditTextSampleSize.String = num2str(round(get(hObject, 'Value')));


% --- Executes during object creation, after setting all properties.
function SliderSampleSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SliderSampleSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function EditTextSampleSize_Callback(hObject, eventdata, handles)
% hObject    handle to EditTextSampleSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditTextSampleSize as text
%        str2double(get(hObject,'String')) returns contents of EditTextSampleSize as a double


% --- Executes during object creation, after setting all properties.
function EditTextSampleSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditTextSampleSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuSampleShape.
function PopUpMenuSampleShape_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuSampleShape (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuSampleShape contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuSampleShape


% --- Executes during object creation, after setting all properties.
function PopUpMenuSampleShape_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuSampleShape (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopUpMenuSampleAlternation.
function PopUpMenuSampleAlternation_Callback(hObject, eventdata, handles)
% hObject    handle to PopUpMenuSampleAlternation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopUpMenuSampleAlternation contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopUpMenuSampleAlternation


% --- Executes during object creation, after setting all properties.
function PopUpMenuSampleAlternation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopUpMenuSampleAlternation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
